Relaeses / Tags
=======================

v1.2.0 Added grasping strategy controllers and grasping pose error estimation
--------------------------------------------------------------------------------

- Added basic python gripper interaces and control modalities from ICRA 22 work
- Added basic code sphinx-docu 
- Minor code fixes to iprove communication rate and stability with gripper

v1.1.2 Added Center of Pressure calculation on gripper and improved communication
--------------------------------------------------------------------------------------

- repaired plotting
- added reading + robot control (previously either / or possible)
- frequencies up to 10Hz (stable at ~5Hz for arrays and ~7Hz for CoP only
- adjusted script and ploting utils for new setups
- allows to run script with available fingers and request


v1.1.1 reworked gripper driver and communication
--------------------------------------------------

Major rework of driver by adjusing API and driver interfae (use gripper_api / gripper_ros module)
partially cleaned and improved performance


v1.0.2 improved DSA support
--------------------------------

- improved DSA communication rate by proper unpacking (up to 2Hz in application)

v1.0.1 BASIC DSA support
--------------------------------

- added DSA reading with full array and flattened lists by @ghuber
- basic Center of pressure caluclation, visualization and pose estimation


v1.0. Initial version
-----------------------

Forked version from google-code: https://code.google.com/p/wsg50-ros-pkg





