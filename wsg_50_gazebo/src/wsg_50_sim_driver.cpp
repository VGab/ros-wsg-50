/* wsg_50_sim_driver
 * Copyright (c) 2012, Robotnik Automation, SLL
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Robotnik Automation, SLL. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \author Marc Benetó (mbeneto@robotnik.es)
 * \brief WSG-50 sim driver.
 */

#include <ros/ros.h>
#include <wsg_50_msgs/Cmd.h>
#include <wsg_50_msgs/Status.h>
#include <wsg_50_msgs/Move.h>
#include <wsg_50_msgs/Incr.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>
#include <std_srvs/Empty.h>
#include <control_msgs/JointControllerState.h>

#define GRIPPER_MAX_OPEN 110.0
#define GRIPPER_MIN_OPEN 0.0

using namespace std;

ros::Publisher vel_pub_r_, vel_pub_l_, g_pub_state, g_pub_moving;
ros::ServiceClient moveSC;
string state_str = "";
bool g_ismoving = false;
float g_goal_position = NAN, g_goal_speed = NAN, g_speed = 10.0, currentOpenning= 0.0, homing_width, tolerance;
unsigned int time_out = 100, ros_rate=50;

bool move(float width_in){
    // process with w.r.t gripper constraints
    float width = width_in;
    if ( width >= GRIPPER_MIN_OPEN && width <= GRIPPER_MAX_OPEN ){
            ROS_DEBUG("Moving to %f position.", width);
    }else{
            width = (float) min(GRIPPER_MIN_OPEN, max((double)width_in, GRIPPER_MAX_OPEN));
            ROS_ERROR("Imposible to move to position: %f. (Width values: [%f - %f]\n\t->choose %f instead",
                       width_in, GRIPPER_MIN_OPEN, GRIPPER_MAX_OPEN, width);
    }
    ros::Duration repub_duration(5.0/float(ros_rate));
    std_msgs::Float64 lCommand, rCommand;
    g_ismoving = true;

    rCommand.data = width / 2000.0;
    lCommand.data = rCommand.data * -1.0;
    state_str += "-moving";
    ROS_DEBUG_STREAM("Initiating: " << state_str);
    for (unsigned int i=0; i <time_out; ++i) {
            vel_pub_r_.publish(rCommand);
            vel_pub_l_.publish(lCommand);            
            if (std::abs(currentOpenning - width) < tolerance) {
                    g_ismoving = false;
                    state_str = "idle";
                    ROS_INFO("Movement succeeded");
                    return true;
            }
            ros::spinOnce();
            repub_duration.sleep();
    }
    state_str = "idle";
    g_ismoving = false;
    ROS_WARN("Movement failed");
    return false;
}

bool action_valid(){
	if (g_ismoving){
		ROS_WARN("gripper is in use, .. retry later");
		return false;
	}
	return true;
}


bool moveSrv(wsg_50_msgs::Move::Request &req, wsg_50_msgs::Move::Response &res)
{
	if (!action_valid()) return false;	
	state_str = "move_srv";
	if ( req.width >= GRIPPER_MIN_OPEN && req.width <= GRIPPER_MAX_OPEN ){
                bool success = move((float) req.width);
                if(success){
                        ROS_DEBUG("Target position reached: %f", currentOpenning);
			return true;
		}
	}else{
		ROS_ERROR("Imposible to move to this position. (Width values: [%f - %f]", 
                                  GRIPPER_MIN_OPEN, GRIPPER_MAX_OPEN);
                return false;
	}
	ROS_WARN("Could not reach position %f. Stuck at %f", req.width, currentOpenning);
	return false;
}

bool moveIncrementallySrv(wsg_50_msgs::Incr::Request &req, wsg_50_msgs::Incr::Response &res)
{
	if (!action_valid()) return false;	
	state_str = "move_incremental_srv";
	if (req.direction == "open"){
		return move(currentOpenning + req.increment);
	}else if (req.direction == "close"){
		return move(currentOpenning - req.increment);
	}
	else{
		ROS_ERROR("Direction invalid. User [open|close]!");
		return false;
	}
}


bool homingSrv(std_srvs::Empty::Request &req, std_srvs::Empty::Request &res)
{
	if (!action_valid()) return false;	
	state_str = "homing";
        bool success = move(homing_width);
        if (success){
            ROS_INFO("Home position reached.");
            return true;
        }
	return false;
}

bool graspSrv(wsg_50_msgs::Move::Request &req, wsg_50_msgs::Move::Request &res)
{
	if (!action_valid()) return false;	
	state_str = "grasping";
        float tmp_w = req.width;
        ROS_INFO("Move to %f", tmp_w);
        bool success = move(tmp_w);
        if (success){
            ROS_INFO("Object grasped");
            return true;
	}
    return true;
}

/** \brief Callback for goal_position topic (in appropriate modes) */
void position_cb(const wsg_50_msgs::Cmd::ConstPtr& msg)
{

    if(g_goal_position == msg->pos)// && flag_moved==true) // to avoid sending the gripper the same command in high frequency
		return;
    g_speed = msg->speed; 
    g_goal_position = msg->pos;

    move(g_goal_position);
}

void state_cb(const control_msgs::JointControllerState::ConstPtr& msg)
{
    currentOpenning = max(GRIPPER_MIN_OPEN, min((float) msg->process_value * 2e3, GRIPPER_MAX_OPEN));

    // ==== Status msg ====
    wsg_50_msgs::Status status_msg;
    // TODO: reimplement status and acc/force?
    status_msg.status = state_str;
    status_msg.width = currentOpenning;
    status_msg.speed = (float) msg->process_value_dot * 1e3;
    //status_msg.acc = ;
    //status_msg.force = ;
    //status_msg.force_finger0 = ;
    //status_msg.force_finger1 = ;

    g_pub_state.publish(status_msg);
}    
    
int main(int argc, char** argv){
	
    ros::init(argc, argv, "wsg_50_sim_driver");
    ros::NodeHandle nh("~");
    ros::Rate rate(50);

    std::string vel_pub_l_Topic, vel_pub_r_Topic, sub_state_topic;
    std_msgs::Bool moving_msg;
	
    nh.param<float>("homing_width", homing_width, 60.0);
    nh.param<float>("delta_e", tolerance, 0.3);
    nh.param<std::string>("vel_pub_l_Topic", vel_pub_l_Topic, "/wsg_50/wsg_50_l/command");
    nh.param<std::string>("vel_pub_r_Topic", vel_pub_r_Topic, "/wsg_50/wsg_50_r/command");
    nh.param<std::string>("sub_state_topic", sub_state_topic, "/wsg_50/wsg_50_r/state");
	
    ros::ServiceServer moveSS = nh.advertiseService("move", moveSrv);
    ros::ServiceServer moveIncrementallySS = nh.advertiseService("move_incrementally", moveIncrementallySrv);
    ros::ServiceServer homingSS = nh.advertiseService("homing", homingSrv);
    ros::ServiceServer graspSS = nh.advertiseService("grasp", graspSrv);
    
    // Subscriber
    ros::Subscriber sub_position, sub_state;
    sub_position = nh.subscribe("goal_position", 5, position_cb);
    sub_state = nh.subscribe(sub_state_topic, 5, state_cb);

    // Publisher
    g_pub_state = nh.advertise<wsg_50_msgs::Status>("status", 50);
    g_pub_moving = nh.advertise<std_msgs::Bool>("moving", 10);
    vel_pub_l_ = nh.advertise<std_msgs::Float64>(vel_pub_l_Topic, 50);
    vel_pub_r_ = nh.advertise<std_msgs::Float64>(vel_pub_r_Topic, 50);

    ROS_INFO("Initialized wsg_50_hw simulation emulator");
    while (ros::ok())
    {
        moving_msg.data = g_ismoving;
        g_pub_moving.publish(moving_msg);
        ros::spinOnce();
        rate.sleep();
    }
} 
