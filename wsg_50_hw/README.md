# WSG 50 HW Ros-driver 

This package mainly consists of 

- WSG 50 C-driver handling the communication with the script
- an Hardware interface class that serves as a hardware-interface within robot-setups / ros-nodes ```wsg_50::Wsg50API```
- a ROS-wrapper that uses the API to serve the internal commands to ROS and publish data as needed ``````wsg_50::Wsg50ROS```
- an example ros node implementation for standalone gripper setups (see [usage](#How-To-Use))


## How-To-Use

The principle functionality of this gripper is best to be seen by launching the example node 
```wsg_50_ip``` via the launch file 

```bash
roslaunch wsg_50_hw wsg_50.launch
```

that loads the default ```tcp``` hardware configuration from the [config](./config) directory.

All parameters in use will be directly printed to the terminal in ```DEBUG``` mode.

```
[ INFO] [1612457969.966485658]: [WSG_50_api] connected to WSG gripper:
	ip:            	192.168.1.20
	port:          	1000
	protocol:      	tcp
	com_mode:      	script
[DEBUG] [1612457969.967455074]: [WSG_50_api] position control to 0.000 [mm] with speed 0.000 [mm/s]
[ INFO] [1612457969.979586150]: [/wsg_50_hw] initialized connection to gripper
[ INFO] [1612457969.991629578]: [/wsg_50_hw] started ROS-gripper interface
[DEBUG] [1612457969.996979936]: [WSG 50] Started gripper node: 
	left joint:          	wsg_50_gripper_base_joint_gripper_left
	right joint:         	wsg_50_gripper_base_joint_gripper_right
	frame_id:            	wsg_50_gripper_base_link"
	loop_hz:      [1/s]  	50
	joint state topic:   	joint_states
	publish_joints:      	1
	publish_full_status: 	1
```

This node will provide the following publishers
```
❯ rostopic list

/wsg_50_hw/joint_states
/wsg_50_hw/moving
/wsg_50_hw/status
```

and subscribers

```
❯ rostopic list

/wsg_50_hw/goal_position
/wsg_50_hw/goal_speed
```

whereas the majority of services wrap the WSG 50 net-interface to ROS

```
❯ rosservice list

/wsg_50_hw/ack
/wsg_50_hw/get_dsa_data
/wsg_50_hw/get_loggers
/wsg_50_hw/grasp
/wsg_50_hw/homing
/wsg_50_hw/move
/wsg_50_hw/move_incrementally
/wsg_50_hw/release
/wsg_50_hw/set_acceleration
/wsg_50_hw/set_force_limit
/wsg_50_hw/set_logger_level
/wsg_50_hw/stop
```

The service ```/wsg_50_hw/get_dsa_data``` is the only extension that triggers a request for DSA-sensor data if possible and returns the current sensor-reading is parsed to the dedicated custom-message (see [wsg_msgs](./wsg_msgs))

An example ipython notebook, that shows how to plot / visualize the data, is to be found in te [data/](./data) driectory alongside the required lua-scripts needed on the gripper-side.

