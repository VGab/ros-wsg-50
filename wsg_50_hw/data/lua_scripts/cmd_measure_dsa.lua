-- Volker Gabler 2021-06
-- adjusted Matrix sending to 16 bit integer to increase update rate (~5Hz)
-- added CoP calculation on gripper to increase update rate

-- Volker Gabler 2021-02
-- introduced functions and on-request DSA parsing to reduce workload
-- and reduce amount of needed scripts

-- Gerold Huber, 2020-01-10
-- change flatten of payload to unpack in appendMatrix() --> 5Hz reached

-- Gerold Huber, 2019-12-06
-- supports fmf finger measurements and dsa fingers via ros nodes
-- fixes error witn ntob(math.nan)
-- dsa fingers require flattening of payload (only 5 nested lists are allowed) --> 1Hz possible

-- Nicolas Alt, 2014-09-04
-- Command-and-measure script
-- Works with extended wsg_50 ROS package
-- Tests showed about 20Hz rate

-- debug commands
debug = false;
verbose_pos_ctrl = false;
verbose_speed_ctrl = false;
verbose_dsa_reading = false;
print_dsa_matrix0 = false;
print_dsa_matrix1 = false;

if (debug) then
    local inspect = require('inspect') -- for debugging of payload
    verbose_pos_ctrl = true;
    verbose_speed_ctrl = true;
    verbose_dsa_reading = true;
end
-- register commands
MSR_ONLY = 0xB0;
POS_CTRL = 0xB1;
SPD_CTRL = 0xB2;
MSR_FINGER = 0xB3;
MSR_FINGER_SPD = 0xB4;
MSR_DSA_COP = 0xB5;
MSR_DSA_COP_SPD = 0xB6;
cmd.register(MSR_ONLY);
cmd.register(POS_CTRL);
cmd.register(SPD_CTRL);
cmd.register(MSR_FINGER);
cmd.register(MSR_FINGER_SPD);
cmd.register(MSR_DSA_COP);
cmd.register(MSR_DSA_COP_SPD);

def_speed = 5;
is_speed = false;
pos_cur = 0;
v_cur = 0;
f_cur = 0;
speed_ctrl_lb = 10;
speed_ctrl_ub = 90;
mc.pid(0.05, 0, 0)  -- Set PID parameters for speed control
force_l = -1; force_r = -1;


-- Prepare data for DSA speed ups
sensor_h = 47.6 -- DSA height in mm
sensor_w = 20.4 -- DSA width in mm
cell_ranges = 3.4 -- cell width in mm (14 cells)
-- needs check if byte parsing needs additional dimension
cell_xy_dsa = {}  -- create cell mm
for i = 1,14 do
    cell_xy_dsa[i] = {}
    for j = 1,6 do        
        cell_xy_dsa[i][j] = {};
        cell_xy_dsa[i][j][1] = cell_ranges * (i - 0.5);
        cell_xy_dsa[i][j][2] = cell_ranges * (j - 0.5);
    end
end



-- Get number of DSA / FMF fingers (sensor array)
num_DSA = 0;
num_FMF = 0;
for i = 0,1 do
    if finger.type(i) == "dsa" then
        num_DSA = num_DSA + 1;
    elseif finger.type(i) == "fmf" then
        num_FMF = num_FMF + 1;
    end
end
num_fingers = num_DSA + num_FMF;

block_dsa_commands = num_DSA == 0;
block_fmf_commands = num_FMF == 0;
printf("Current configuration:\n")
printf("   #DSA finger(s):\t%d\n", num_DSA)
printf("   #FMF finger(s):\t%d\n", num_FMF)


-- Helper function: Print the content of the given matrix m:
-- (from dsa_reference_manual)
function printMatrix( m )
    local x, y;
    for y=1,#m[1] do
       for x=1,#m do
           printf( "%4d ", m[x][y] );
        end;
        printf( "\n" );
    end;
end;

-- Helper function: Print the content of the given tensor t:
function printTensor(t, inner_dim)
    local x, y, d;
    for y=1,#t[1] do
        for x=1,#t do
           printf( "(")
           for d=1, inner_dim do
                if d < inner_dim then
                    printf( "%2.2f, ", t[x][y][d] );
                else
                    printf( "%2.2f), ", t[x][y][d] );
                end;
            end;
        end;
        printf("\n");
    end;
end;

-- helper function to print center of pressure data
function printCoPData(cop_data)
    if #cop_data >1 then
        printf("preassure sum:\ncenter: %f, %f, data-columns: %d\n", cop_data[2], cop_data[3], cop_data[4])
        for i=1,cop_data[4] do
            y = cop_data[4+3*i-2];
            x_m = cop_data[4+3*i-1];
            x_max = cop_data[4 + 3*i];
            printf("[(%d,%d) -> (%d, %d)]\n", x_m, y, x_max, y);
        end
    end
end;


-- Flatten function, to avoid problems with #list in payload <=5 !!!
function flatten(list)
  if type(list) ~= "table" then return {list} end
  local flat_list = {}
  for _, elem in ipairs(list) do
    for _, val in ipairs(flatten(elem)) do
      flat_list[#flat_list + 1] = val
    end
  end
  return flat_list
end


-- Forward payload command to position finger control
function pos_ctrl(payload, verbose)
    cmd_width = bton({payload[2],payload[3],payload[4],payload[5]});
    cmd_speed = math.abs(bton({payload[6],payload[7],payload[8],payload[9]}));
    if pos_cur ~= cmd_width then
        if verbose then
            printf("Send position control command p=%f [mm], |v|=%f [mm/s]\n", cmd_width, cmd_speed)
        end
       if mc.busy() then
            mc.stop();
       end
       mc.move(cmd_width, math.abs(cmd_speed), 0);
    end
end

-- Forward payload command to position finger control
function speed_ctrl_cmd(payload, verbose)
    -- do_speed = hasbit(payload[1], 0x02);
    cmd_speed = bton({payload[6],payload[7],payload[8],payload[9]});
    is_speed = true;
    def_speed = cmd_speed;
    mc.speed(cmd_speed);
end


function check_spd_ctrl()
    if  mc.blocked() and is_speed then        
        if (pos_cur <= speed_ctrl_lb and def_speed < 0) or (pos_cur >= speed_ctrl_ub and def_speed > 0 )then
            print("stop");
            mc.stop();
            is_speed = false;
        end
    end
end


function msrDSA(return_payload)    
    if num_DSA > 1 then
        -- appendMatrix(return_payload, finger.data(0).frame);
        -- appendMatrix(return_payload, finger.data(1).frame);
        appendMatrix16(return_payload, finger.data(0).frame);
        appendMatrix16(return_payload, finger.data(1).frame);        
    end
end

function DEBUG_msrDSA(return_payload)
    t0 = os.clock()
    t1 = os.clock()
    t2 = os.clock()
    if num_DSA > 0 then
        if print_dsa_matrix0 then
            printMatrix(finger.data(0).frame)
        end
        appendMatrix16(return_payload, finger.data(0).frame);        
        t1 = os.clock()
        if num_DSA > 1 then
            if print_dsa_matrix1 then
                printMatrix(finger.data(1).frame)
            end
            appendMatrix16(return_payload,finger.data(1).frame);
            t2 = os.clock()
        end
    end
    printf("parsing took %.2f s, mat1: %.2f s, mat2: %.2f\n",
          (t2-t0), (t1-t0), (t2-t1))
end

        
function msrFNF(return_payload)   
    if num_FNF > 0 then
        appendNumber(return_payload,finger.data(0));
        if num_FNF > 1 then
            appendNumber(return_payload, finger.data(1));
        end
    end
end


function int16_to_bytes(n)
  if n >= 65536 then error(n.." is too large",2) end
  if n < 0 then error(n.." is too small",2) end
  return n%256, (math.modf(n/256))%256
end


-- calculate Center of Pressure, and return range of pressure circles
-- returns list
-- 0 -> sum_{i,j} c_ij
-- 1 -> r_cop_x  float center x
-- 2 -> r_cop_y  float center y
-- 3 -> n_edges  int num edges
-- 4-n_edges * 3 --> (y, x_min, x_max) integer edge points
function calc_cop(matrix)
    return_value = {};
    x_cop = 0.0;
    y_cop = 0.0;
    x_axes = {};
    y_min = {};
    y_max = {};

    dsa_norm = 0;
    x_tmp_sum = 0;
    y_tmp_sum = 0;        
    for x=1,#matrix do
       checked = false;
       for y=1,#matrix[1] do
            dsa_norm = dsa_norm + matrix[x][y];
            if matrix[x][y] > 0 then
                if not checked then
                    y_min[#y_min + 1] = y;
                    x_axes[#x_axes + 1] = x;
                    checked = true;
                end                
                y_max[#x_axes] = y;
                x_tmp_sum = x_tmp_sum + matrix[x][y] * cell_xy_dsa[y][x][1];
                y_tmp_sum = y_tmp_sum + matrix[x][y] * cell_xy_dsa[y][x][2];
            end
        end
    end

    return_value[1] = dsa_norm;
    if dsa_norm > 0 then
        return_value[2] = x_tmp_sum / dsa_norm;
        return_value[3] = y_tmp_sum / dsa_norm;
        return_value[4] = #y_min;
        for i=1,#y_min do
            return_value[#return_value + 1] = x_axes[i];
            return_value[#return_value + 1] = y_min[i];
            return_value[#return_value + 1] = y_max[i];
        end
    end
    return return_value;
end


-- add pressure counters for eased read-out
-- each finger reports number of filled columns [0,6] -> encoded as 6 bit
--  0000 --> no data
function add_pressure_count(payload, p_data)
    if #p_data > 3 then
        payload[#payload + 1] = math.pow(2, p_data[4])
    else
        payload[#payload + 1] = 0;
    end    
end


function add_pressure_data(payload, data)
    if #data > 3 then
        appendNumber(payload, data[2]);
        appendNumber(payload, data[3]);
        for i=5,#data do
            payload[#payload+1] = data[i] % 256;           
        end
    end
end
        

-- adds concise pressure data to payload for both fingers
function appendCopData(payload)
    if finger.type(0) == "dsa" and finger.type(1) == "dsa" then
        data0 = calc_cop(finger.data(0).frame, 0)
        data1 = calc_cop(finger.data(1).frame, 1)
        if verbose_dsa_reading then
            printCoPData(data0, 0);
            printCoPData(data1, 1);
        end;
        -- add total pressure data from arrays        
        appendNumber(payload, data0[1]);
        appendNumber(payload, data1[1]);
        -- add pressure counts
        add_pressure_count(payload, data0);
        add_pressure_count(payload, data1);       
        -- add content per array
        add_pressure_data(payload, data0);
        add_pressure_data(payload, data1);
    end
end


-- send functions
function send_default_return(id)
    return_payload = {};
    appendNumber(return_payload, pos_cur);
    appendNumber(return_payload, v_cur);
    appendNumber(return_payload, f_cur);
    cmd.send(id, etob(E_SUCCESS), state % 256, return_payload)      
end
    

function send_finger_data(id)
    return_payload = {};
    appendNumber(return_payload, pos_cur);
    appendNumber(return_payload, v_cur);
    appendNumber(return_payload, f_cur);
    if num_DSA > 0 then
        if verbose_dsa_reading then
            DEBUG_msrDSA(return_payload);
        else
            msrDSA(return_payload);
        end
    elseif num_FNF > 0 then
       msrFNF(return_payload);
    end
    cmd.send(id, etob(E_SUCCESS), state % 256, return_payload);   
end


function send_sparse_finger_data(id)    
    return_payload = {};
    appendNumber(return_payload, pos_cur);
    appendNumber(return_payload, v_cur);
    appendNumber(return_payload, f_cur);        
    appendCopData(return_payload);
    cmd.send(id, etob(E_SUCCESS), state % 256, return_payload);
end


function process()
    pos_cur = mc.position();
    v_cur = mc.speed();
    f_cur = mc.aforce();
    if num_fingers == 2 then  
        force_l = finger.data(0);
        force_r = finger.data(1);
    end
    if cmd.online() then
        id, payload = cmd.read();    
        -- check command
        if id == POS_CTRL or id == MSR_FINGER or id == MSR_DSA_COP then
            pos_ctrl(payload, verbose_pos_ctrl);
        elseif id == SPD_CTRL or id == MSR_FINGER_SPD or id == MSR_DSA_COP_SPD then
            speed_ctrl_cmd(payload, verbose_speed_ctrl);
        end
        if (is_speed) then
            check_spd_ctrl();
        end
        
        -- ==== Get measurements ====
        state =  gripper.state();

       if id == MSR_FINGER or id == MSR_FINGER_SPD then
            send_finger_data(id)
       elseif id == MSR_DSA_COP_SPD or id == MSR_DSA_COP then
            send_sparse_finger_data(id)
       else
            send_default_return(id)
        end
    end
end

function appendMatrix32(payload,matrix) -- timing: ~0.053s
    -- sensor matrix is [rows x cols] = [6 x 14]
    -- flip columns / row definition to fit printout
    for y=1,#matrix[1] do
        for x=1,#matrix do
            payload[#payload+1],payload[#payload+2],payload[#payload+3],payload[#payload+4] = unpack(ntob(matrix[x][y]));
       end
    end
end

function appendMatrix16(payload,matrix) -- timing: ~0.053s
    -- sensor matrix is [rows x cols] = [6 x 14]
    -- flip columns / row definition to fit printout
    for y=1,#matrix[1] do
        for x=1,#matrix do
            payload[#payload+1],payload[#payload+2] = int16_to_bytes(matrix[x][y]);
       end
    end
end


function appendInt16(payload,number)
        payload[#payload+1],payload[#payload+2] = int16_to_bytes(number);
end

function appendNumber(payload,number)
    payload[#payload+1],payload[#payload+2],payload[#payload+3],payload[#payload+4] = unpack(ntob(number));                    
end

function test()
    payload={}
    force_l= -1
    
    --
    tic = os.clock()
    printf('----------------TEST OUT--------------------------------\n')
    if finger.type(0) == "dsa" then
        force_l= finger.data(0)        
        printf("array size rows: %d columns: %d\n",#force_l.frame,#force_l.frame[1])
        cur_len = #payload
        t0 = os.clock()
        appendMatrix32(payload,finger.data(0).frame);
        printf('appendMatrix32->added %d bytes within:\t%.3f [s]\n', #payload - cur_len, os.clock()- t0)
        cur_len = #payload
        t0 = os.clock()
        appendMatrix16(payload,finger.data(0).frame);
        printf('appendMatrix16->added %d bytes within:\t%.3f [s]\n', #payload - cur_len, os.clock()- t0)
    elseif finger.type(0) == "fmf" then
        force_l= finger.data(0)
        appendNumber(payload,force_l);
    end
    if finger.type(1) == "dsa" then       
        printCoPData(calc_cop(finger.data(1).frame, 1));
        tic_cop = os.clock()
        appendCopData(payload, 1);
        print(string.format("caclulated center of pressure (F1) within:\t%.3f [s]", os.clock()-tic_cop) )         
    end
    printf("length of total payload:\t\t%d\n", #payload * num_fingers)
    toc = os.clock()
    print(string.format("total elapsed time for both fingers:\t\t%.3f [s]",toc-tic))
    printf('--------------------------------------------------------\n')
end

test()

while true do
    if cmd.online() then
        if not pcall(process) then
            print("Error occured")
            sleep(100)
        end
    else
        print("no driver connected...")
        sleep(10000)
    end     
end

