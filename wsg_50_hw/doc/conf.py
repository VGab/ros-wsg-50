# -*- coding: utf-8 -*-
#!/usr/bin/env python3
import os
import sys
import sphinx_bootstrap_theme

sys.path.insert(0, os.path.abspath('../src'))
sys.path.insert(0, os.path.abspath('../scripts'))

# -- Project information -----------------------------------------------------
release = "public"
project = "wsg_50_hw documentation"
copyright = "Copyright (C) 2014-2021 TUM LSR"
author = "v.gabler@tum.de"
version = "1.2.0"
# -- General configuration ---------------------------------------------------
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',    
    'sphinx.ext.mathjax',
    'sphinxcontrib.napoleon',    
    'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    'sphinx.ext.todo',
    'sphinx_click.ext',    
]
# napoleon settings
napoleon_include_init_with_doc = True
napoleon_use_ivar = True
napoleon_use_param = False
napoleon_use_rtype = False

source_suffix =  {
    '.rst': 'restructuredtext',
    '.md': 'markdown'
}
master_doc = 'index'
language = None
exclude_patterns = []
pygments_style = 'sphinx'
add_module_names = False
show_authors = True

# -- Options for HTML output -------------------------------------------------
html_domain_indices = False
html_use_index = True
html_split_index = True
html_show_sourcelink = True
html_help_basename = '{} help'.format(project)
html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()
html_sidebars = {
    'content':['localtoc.html'],
    'docu/nodes':['localtoc.html'],
}

# -- Options for LaTeX output ------------------------------------------------
man_pages = [
    (master_doc, project, '{} Documentation'.format(project),
     [author], 1)
]
todo_include_todos = False
