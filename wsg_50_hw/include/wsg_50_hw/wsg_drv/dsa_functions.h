//======================================================================
/**
 *  @file
 *  dsa_functions.h
 *
 */
//======================================================================


#ifndef DSA_FUNCTIONS_H_
#define DSA_FUNCTIONS_H_

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <cmath>
#include <array>

#include "functions.h"
#include "constants.h"
#include "wsg_50_hw/wsg_drv/common.h"
#include "wsg_50_hw/wsg_drv/constants.h"
#include "wsg_50_hw/wsg_drv/cmd.h"
#include "wsg_50_hw/wsg_drv/msg.h"

typedef std::array<std::array<int32_t, DSA_COLS>, DSA_ROWS> DsaMat;
typedef std::pair<DsaMat, DsaMat> DsaData;

#define DSA_CENTER_X 23.8
#define DSA_CENTER_Y 10.2

int dsa_script_measure_move(const ScriptCmd &cmd_type, const float& cmd_width, const float cmd_speed, gripper_response & info);
  

int dsa_script_measure_move(const ScriptCmd &cmd_type, const float& cmd_width, const float cmd_speed,
                            gripper_response & info, DsaData &dsa_data);

int dsa_script_measure_move(const ScriptCmd &cmd_type, const float& cmd_width, const float cmd_speed,
                            gripper_response & info,
                            std::pair<std::pair<float, float>, std::pair<float, float>>  &m_dsa_cop,
                            std::pair<int, int> &dsa_prsr_sum,
                            std::pair<std::vector<std::array<int, 3>>, std::vector<std::array<int, 3>>> &edges);

#endif /* FUNCTIONS_H_ */
