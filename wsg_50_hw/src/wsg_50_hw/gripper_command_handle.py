#!/usr/bin/env python

import numpy as np

import rospy
from std_msgs.msg import Float32
from std_srvs.srv import Empty, EmptyRequest, SetBool
from wsg_50_msgs.msg import Cmd, SetDsa
from wsg_50_msgs.srv import GetDsa, GetDsaCop, GetDsaCopRequest

from wsg_50_hw.gripper_state_handle import *
from wsg_50_hw.dsa_conversions import *
from wsg_50_hw.utils import fix_ns, fix_prefix

__all__ = ["GripperCommandHandle", "DsaGripperCommandHandle"]


class BaseCommander:
    _MAX_SPEED = 420.0

    def __init__(self, wsg_node_name, **kwargs):
        wsg_node_name = fix_ns(wsg_node_name)
        gripper_pos_topic = f'{wsg_node_name}goal_position'
        gripper_vel_topic = f'{wsg_node_name}goal_speed'
        gripper_ack_srv = f'{wsg_node_name}ack'
        gripper_home_srv = f'{wsg_node_name}homing'
        self._vel_pub = rospy.Publisher(gripper_vel_topic, Float32, queue_size=10)
        self._pos_pub = rospy.Publisher(gripper_pos_topic, Cmd, queue_size=10)
        self._ack_srv = rospy.ServiceProxy(gripper_ack_srv, Empty)
        self._homing_srv = rospy.ServiceProxy(gripper_home_srv, Empty)
        self._ack_trials = int(10.0 // 5)

    def send_pos(self, p, speed=100.0, si=False):
        """send_pos

        Args:
            p (float): desired gripper opening width in m / mm
            speed (float, optional): closing / opening speed in mm/s. Defaults to 100.0.
            si (bool, optional): transforms m to mm if p is provided in m. Defaults to False.
        """
        if si and p < 0.11:
            p *= 1e3
        self._pos_pub.publish(Cmd(mode="", pos=p, speed=speed))

    def set_vel(self, v, si=False):
        """set_vel
        send closing velocity to gripper.

        Gripper is commanded in mm / s, thus if :math:`v` is provided as SI-unit, the velocity is adjusted before being sent

        Args:
            v (float): desired closing / opening speed of the gripper
            si (bool, optional): flag to convert velocity from m to mm before sending. Defaults to False.
        """
        if si and 0 < abs(v) < 0.42:
            v *= 1e3
        v = np.sign(v) * np.clip(abs(v), 0, 420)
        self._vel_pub.publish(Float32(v))

    def scale_velocity(self, s, closing=True):
        v_scale = np.clip(np.abs(s), 0, 1) * self._MAX_SPEED
        if closing:
            return -v_scale
        return v_scale

    def ack(self):
        for t in range(self._ack_trials):
            try:
                self._ack_srv(EmptyRequest())
                return
            except rospy.service.ServiceException:
                rospy.sleep(0.2)

    def reset(self, dt=0.1):
        self.ack()
        rospy.sleep(dt)
        try:
            self._homing_srv(EmptyRequest())
        except rospy.service.ServiceException as e:
            rospy.logerr(f"could not home gripper due to {e} -> manual reset needed")
        rospy.sleep(dt)
        self.set_vel(0.0)


class GripperCommandHandle(GripperStateHandle, BaseCommander):
    """
    Extend State handle by additional command interfaces

    Allows to set position / velocity via eased API structure
    """

    def __init__(self, wsg_node_name):
        GripperStateHandle.__init__(self, wsg_node_name)
        BaseCommander.__init__(self, wsg_node_name)


class DsaGripperCommandHandle(DsaGripperStateHandle, BaseCommander):

    def __init__(self, wsg_node_name):
        wsg_node_name = fix_ns(wsg_node_name)
        DsaGripperStateHandle.__init__(self, wsg_node_name)
        BaseCommander.__init__(self, wsg_node_name)
        dsa_service = f'{wsg_node_name}get_dsa_data'
        dsa_cop_service = f'{wsg_node_name}get_dsa_cop'
        dsa_flag_topic = f'{wsg_node_name}set_dsa_read'
        self._dsa_flag_pub = rospy.Publisher(dsa_flag_topic, SetDsa, queue_size=100)
        self._dsa_srv = rospy.ServiceProxy(dsa_service, GetDsa)
        self._dsa_cop_srv = rospy.ServiceProxy(dsa_cop_service, GetDsaCop)
        self._dsa_active = False

    @classmethod
    def from_ros(cls, ros_param_prefix="~"):
        ros_param_prefix = fix_prefix(ros_param_prefix)
        return cls(rospy.get_param(f"{ros_param_prefix}wsg_node_name"))

    def enable_dsa(self, cop_only=False):
        self._dsa_flag_pub.publish(SetDsa(enable_dsa=True, cop_only=cop_only))
        self._dsa_active = True

    def disable_dsa(self):
        self._dsa_flag_pub.publish(SetDsa(enable_dsa=False, cop_only=False))
        self._dsa_active = False
        self.dsa_sum = [0, 0]

    @property
    def dsa_active(self):
        return self._dsa_active

    def __str__(self):
        base_str = super().__str__()
        base_str += f"\nDSA active:\t{self._dsa_active}"
        return base_str

    def get_new_dsa(self):
        """Force DSA update via the service API

        Args:
            timout(int, optional): timeout for service request in ms. Defaults to 10000, i.e. 10 s.
        """
        if self._dsa_active:
            rospy.logerr("calling service while DSA reading is active is not recommended, aborting")
            return
        try:
            for i, DSA in enumerate(get_dsa_matrices(self._dsa_srv)):
                self._dsa_msr[i] = DSA
        except TypeError:
            rospy.logerr("could not get new data")

    def get_new_dsa_cop(self, timeout=10000):
        """Force DSA center of pressure update via the service API

        Args:
            timout(int, optional): timeout for service request in ms. Defaults to 10000, i.e. 10 s.
        """
        if self._dsa_active:
            rospy.logerr("calling service while DSA reading is active is not recommended, aborting")
            return
        try:
            cop_data_res = self._dsa_cop_srv(GetDsaCopRequest(timeout=timeout))
            for i in range(2):
                cop_data = getattr(cop_data_res, f"dsa_cop{i}")
                self._edges[i] = np.vstack((cop_data.edge_x, cop_data.edge_y))
                self._cop_msg[i] = np.r_[cop_data.x, cop_data.y]
                self.dsa_sum[i] = cop_data.sum
        except TypeError:
            rospy.logerr("could not get new data")


